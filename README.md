Table of Contents
=========================
1. [Introduction](#introduction)
2. [Python Coding Test](#test)
2.1. [Part I](#part1)
2.2. [Part II](#part2)
2.2.1. [Part II.1](#part21)
2.2.2. [Part II.2](#part22)


# 1. Introduction <a name="introduction"></a>


- This repository contains the answers for the Mango Solutions Python Coding Test
- The code was written in Python 2.7 and requires the following external library:

    - `numpy` 


# 2. Python Coding Test <a name="test"></a>

## 2.1 Part I <a name="part1"></a>

The code used to get the following output can be found in `part1.py`.

        06/18/2017 08:02:54 PM :: Create a Numpy array of length 26, containing samples from a normal distribution (mean=0 and std=1).
        06/18/2017 08:02:54 PM :: 
        [-0.895  0.432 -1.134  1.383 -1.828  1.312 -0.602  0.858  0.692 -1.079
         -0.890 -0.553  0.526  1.114 -1.977 -0.843  0.494  0.495 -0.786 -0.218
          2.349 -0.155  0.568  1.587  0.689  0.334]
        06/18/2017 08:02:54 PM :: Min: -1.9772480592
        06/18/2017 08:02:54 PM :: Max: 2.34942001222
        06/18/2017 08:02:54 PM :: Median: 0.38289957836
        06/18/2017 08:02:54 PM :: 25th percentile: -0.828888341173
        06/18/2017 08:02:54 PM :: 75th percentile: 0.38289957836
        06/18/2017 08:02:54 PM :: 10x10 Numpy array of samples from a Poisson distribution (lambda=1)
        06/18/2017 08:02:54 PM :: 
        [[0 1 1 1 0 1 2 3 1 2]
         [1 3 1 1 0 1 0 1 1 0]
         [0 2 0 4 0 0 1 3 1 0]
         [1 3 1 0 1 1 0 1 0 2]
         [1 1 0 0 2 1 0 1 1 0]
         [1 0 1 3 0 0 0 0 1 0]
         [1 0 1 2 1 2 0 0 1 0]
         [2 1 1 1 0 1 1 0 1 3]
         [0 2 2 3 0 0 1 1 2 0]
         [2 1 1 0 0 0 1 0 0 0]]
        06/18/2017 08:02:54 PM :: Maximum value of each row of the matrix: 
        [3 3 4 3 2 3 2 3 3 2]
        06/18/2017 08:02:54 PM :: Storing both array objects in a list structure...
        06/18/2017 08:02:54 PM :: Length of each component of the list: 26, 10
        06/18/2017 08:02:54 PM :: Function to output the maximum value of each row of a matrix: 
        [3 3 4 3 2 3 2 3 3 2]

## 2.2 Part II <a name="part2"></a>

## 2.2.1 Part II.1 <a name="part21"></a>

The function that simulates numbers from a given distribution can be found in `part1.py`.

Heareafter the output of the created function for the three distributions.


        06/18/2017 08:08:08 PM :: BINOMIAL
        06/18/2017 08:08:08 PM :: [10  5  6  5  5 10  7  7  7  4  5]
        06/18/2017 08:08:08 PM :: NORMAL
        06/18/2017 08:08:08 PM :: 
        [ 1.528  4.409  1.692  1.909 -1.956  3.104 -1.557  3.187 -0.907  0.596  0.400]
        06/18/2017 08:08:08 PM :: POISSON
        06/18/2017 08:08:08 PM :: 
        [[2 2 6 0 2 2]
         [1 2 3 2 2 1]
         [2 3 2 4 3 1]
         [3 1 2 3 3 0]
         [3 0 0 1 0 1]
         [4 3 3 3 5 3]
         [1 3 1 5 0 1]
         [3 3 1 3 2 2]
         [0 3 2 2 2 2]
         [0 0 0 1 2 3]
         [2 1 3 2 1 3]]

## 2.2.2 Part II.2 <a name="part22"></a>

How to install the created `sim_dist` package?

1. clone the repository: `git clone https://hpcosta@bitbucket.org/hpcosta/mangosolutions.git`

2. go to the folder mangosolutions `cd mangosolutions`

3. install the package: `sudo pip install .`

4. import the `sim_dist` and call the function `simulate_distributions`: 

		$ python

        >> import sim_dist
        
        >> sim_dist.simulate_distributions((11, 6), 'poisson', lam=2)
        
        	array([[ 0,  1,  1,  2,  4,  2],
               [ 4,  0,  2,  4,  1,  4],
               [ 0, 10,  0,  3,  2,  3],
               [ 6,  3,  0,  2,  3,  3],
               [ 5,  3,  2,  1,  2,  1],
               [ 0,  3,  3,  4,  2,  4],
               [ 0,  1,  0,  2,  2,  3],
               [ 1,  1,  1,  3,  5,  0],
               [ 1,  2,  4,  3,  4,  4],
               [ 3,  2,  1,  2,  3,  4],
               [ 1,  3,  1,  0,  3,  1]])


Note: an example of its usage con also be found in the `part1.py` file.


# Contact Information

> Name: Hernani Costa

> Email: hernanimax@gmail.com

> Last update: 18/06/2017