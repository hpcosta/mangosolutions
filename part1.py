#!/usr/bin/env python
# -- coding: UTF-8 --

"""
Short Description:

Created on 15/06/2017

:author: hpcosta
"""

import logging
import numpy as np

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p ::')


class Part1():
	""" docstring """

	def __init__(self):
		"""Constructor for part1"""
		np.set_printoptions(formatter={'float': '{: 0.3f}'.format})
		self.__part1()

	def __part1(self):
		# PART I.1
		mean, std = 0, 1
		norm_dis_array = np.random.normal(mean, std, 26)
		logging.info(
			'Create a Numpy array of length 26, containing samples from a normal distribution (mean=0 and std=1).')
		logging.info(norm_dis_array)

		# PART I.2
		min = np.nanmin(norm_dis_array)
		max = np.nanmax(norm_dis_array)
		median = np.median(norm_dis_array)
		q1 = np.percentile(norm_dis_array, 25)
		q3 = np.percentile(norm_dis_array, 50)
		logging.info('Min: ' + str(min))
		logging.info('Max: ' + str(max))
		logging.info('Median: ' + str(median))
		logging.info('25th percentile: ' + str(q1))
		logging.info('75th percentile: ' + str(q3))

		# PART I.3
		poisson_dis_matrix = np.random.poisson(lam=1, size=(10, 10))
		logging.info('10x10 Numpy array of samples from a Poisson distribution (lambda=1)')
		logging.info(poisson_dis_matrix)

		# PART I.4
		logging.info('Maximum value of each row of the matrix: ' + str(poisson_dis_matrix.max(1)))

		# PART I.5
		logging.info('Storing both array objects in a list structure...')
		l = [norm_dis_array, poisson_dis_matrix]

		# PART I.6
		logging.info('Length of each component of the list: %s, %s', len(l[0]), len(l[1]))

		# PART I.7
		logging.info('Function to output the maximum value of each row of a matrix: ' + str(
			self.__max_value(poisson_dis_matrix)))

		logging.info('PART II')
		# Part II.1
		logging.info('BINOMIAL')
		logging.info(self.simulate_distributions(11.2, 'binomial', n_trials=12, prob=0.5))
		logging.info('NORMAL')
		logging.info(self.simulate_distributions(11.2, 'normal', std=2, mean=1))
		logging.info('POISSON')
		logging.info(self.simulate_distributions((11.4, 6.4), 'poisson', lam=2))

		# Part II.2
		# testing Part II.2
		import sim_dist
		logging.info('II.2')
		logging.info(sim_dist.simulate_distributions(11.2, 'binomial', n_trials=12, prob=0.5))
		logging.info('BINOMIAL')
		logging.info(sim_dist.simulate_distributions(11.2, 'binomial', n_trials=12, prob=0.5))
		logging.info('NORMAL')
		logging.info(sim_dist.simulate_distributions(11.2, 'normal', std=2, mean=1))
		logging.info('POISSON')
		logging.info(sim_dist.simulate_distributions((11, 6), 'poisson', lam=2))

	def simulate_distributions(self, n_samples, distribution, mean=None, std=None, lam=None, n_trials=None, prob=None):
		"""
		Generates random numbers for a given distribution.
		Depending on the distribution, different parameters will be required.
		:param n_samples: number of samples to be generated. int or tuple of ints
		:type n_samples: int or tuple
		:param distribution: distribution, which can be Normal, Poisson or Binomial
		:type distribution: str
		:param mean: float mean ("centre") of the distribution.
		:type mean: float
		:param std: float Standard deviation (spread or "width") of the distribution.
		:type std: float
		:param lam: float Expectation of interval, should be >= 0.
		:type lam: float
		:param n_trials: float (but truncated to an integer) parameter, >= 0.
		:type n_trials: float
		:param prob: float parameter, >= 0 and <=1.
		:type prob: float
		:return: array with random numbers for a given distribution
		:rtype: numpy array or matrix
		"""
		self.__is_not_used()

		distribution = distribution.lower()

		# Binomial
		if distribution == 'binomial':
			if isinstance(prob, (int, float)) and isinstance(n_trials, (int, float)) and isinstance(n_samples, (int, float)):
				if n_trials is not None and prob is not None:
					n_trials = int(n_trials)
					if n_trials < 0:
						logging.error('ERROR: n_trials needs to be >= 0')
						return None
					else:
						if 0 <= prob <= 1:
							return np.random.binomial(n_trials, prob, n_samples)
						else:
							logging.error('ERROR: prob needs to be a value between 0 and 1')
							return None
			else:
				logging.error('ERROR: n_trials and prob need to be int or float ')
				return None
		# NORMAL
		if distribution == 'normal':
			if isinstance(mean, (int, float)) and isinstance(std, (int, float)) and isinstance(n_samples, (int, float, tuple)):
				return np.random.normal(mean, std, n_samples)
			else:
				logging.error('ERROR: mean and std need to be int or float and n_samples need to be either int, float or tuple')
				return None

		# POISSON
		if distribution == 'poisson':
			if isinstance(lam, (int, tuple)) and isinstance(n_samples, (int, float, tuple)):
				if lam >= 0:
					return np.random.poisson(lam=lam, size=n_samples)
				else:
					logging.error('ERROR: lam needs to be >= 0')
					return None
			else:
				logging.error('ERROR: n_samples needs to be int, float or tuple of ints and lam int or float')
				return None

	def __max_value(self, matrix):
		"""
		Returns the max value for a given numpy array
		:param matrix: numpy array
		:return: max value for a given numpy array
		"""
		self.__is_not_used()
		return matrix.max(1)

	def __is_not_used(self):
		pass


# FOR TESTING PURPOSES
if __name__ == '__main__':
	Part1()
