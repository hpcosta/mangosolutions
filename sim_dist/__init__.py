import numpy as np


def simulate_distributions(n_samples, distribution, mean=None, std=None, lam=None, n_trials=None, prob=None):
	"""
	Generates random numbers for a given distribution.
	Depending on the distribution, different parameters will be required.
	:param n_samples: number of samples to be generated. int or tuple of ints
	:type n_samples: int or tuple
	:param distribution: distribution, which can be Normal, Poisson or Binomial
	:type distribution: str
	:param mean: float mean ("centre") of the distribution.
	:type mean: float
	:param std: float Standard deviation (spread or "width") of the distribution.
	:type std: float
	:param lam: float Expectation of interval, should be >= 0.
	:type lam: float
	:param n_trials: float (but truncated to an integer) parameter, >= 0.
	:type n_trials: float
	:param prob: float parameter, >= 0 and <=1.
	:type prob: float
	:return: array with random numbers for a given distribution
	:rtype: numpy array or matrix
	"""

	distribution = distribution.lower()

	# Binomial
	if distribution == 'binomial':
		if isinstance(prob, (int, float)) and isinstance(n_trials, (int, float)) and isinstance(n_samples, (int, float)):
			if n_trials is not None and prob is not None:
				n_trials = int(n_trials)
				if n_trials < 0:
					return None
				else:
					if 0 <= prob <= 1:
						return np.random.binomial(n_trials, prob, n_samples)
					else:
						return None
		else:
			return None
	# NORMAL
	if distribution == 'normal':
		if isinstance(mean, (int, float)) and isinstance(std, (int, float)) and isinstance(n_samples, (int, float, tuple)):
			return np.random.normal(mean, std, n_samples)
		else:
			return None

	# POISSON
	if distribution == 'poisson':
		if isinstance(lam, (int, tuple)) and isinstance(n_samples, (int, float, tuple)):
			if lam >= 0:
				return np.random.poisson(lam=lam, size=n_samples)
			else:
				return None
		else:
			return None
