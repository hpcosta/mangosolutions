from setuptools import setup

setup(name='simulate_distributions',
      version='0.1',
      description='Simulates numbers from a given distribution (Normal, Binomial and Poisson).',
      url='https://bitbucket.org/hpcosta/mangosolutions',
      author='Hernani Costa',
      author_email='hernanimax@gmail.com',
      license='MangoSolutions',
      packages=['sim_dist'],
      install_requires=["numpy"],  # Installing numpy as a dependency
      setup_requires=["numpy"],
      zip_safe=False)
